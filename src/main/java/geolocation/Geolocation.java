package geolocation;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;
import com.lynden.gmapsfx.service.geocoding.GeocoderStatus;
import com.lynden.gmapsfx.service.geocoding.GeocodingResult;
import com.lynden.gmapsfx.service.geocoding.GeocodingService;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;

/**
 *
 * @author VaseiliosTzinis
 */
public class Geolocation {

    GoogleMap map;
    GoogleMapView mapView;
    GeocodingService geocodingService;
    Scene scene;
    Marker marker;
    MarkerOptions markerOptions;
    JFXPanel jfxPanel;
    double lat;
    double longt;

    public Geolocation(double lat, double longt) {
        this.lat = lat;
        this.longt = longt;
        init();
    }

    public void init() {
        jfxPanel = new JFXPanel();
        jfxPanel.setSize(720, 720);
        jfxPanel.setLocation(0, 0);
        Platform.runLater(() -> {
            mapView = new GoogleMapView();
            mapView.addMapInializedListener(() -> {
                LatLong center = new LatLong(lat, longt);
                MapOptions options = new MapOptions()
                        .center(center)
                        .zoom(16)
                        .overviewMapControl(false)
                        .panControl(false)
                        .rotateControl(false)
                        .scaleControl(false)
                        .streetViewControl(false)
                        .zoomControl(false)
                        .mapType(MapTypeIdEnum.ROADMAP);
                map = mapView.createMap(options);

                markerOptions = new MarkerOptions();
                markerOptions.position(center)
                        .visible(true);
                marker = new Marker(markerOptions);
                map.addMarker(marker);
            });
            scene = new Scene(mapView);
            jfxPanel.setScene(scene);
        });
    }

    public JFXPanel getJFXPanel() {
        return jfxPanel;
    }
}
