package ui;

import controller.Controller;
import java.awt.Cursor;
import model.Album;
import model.Picture;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Arrays;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import model.User;
import repository.SettingsRepository;
import static controller.Controller.TAG_DEFAULT_FOLDER;

/**
 *
 * @author VaseiliosTzinis
 */
public class HomePage extends javax.swing.JFrame {

    Controller controller;

    /**
     * Initializes the HomePage.
     */
    public HomePage() {
        initComponents();
        this.setLocationRelativeTo(null);
        controller = new Controller();
        spane.getVerticalScrollBar().setUnitIncrement(33);
        connectingUser(controller.getUser().isLogedIn());
        initPanel(Controller.LOADED_LOCAL_PICTURES);
    }

    /**
     * Builds the panel according to panelLoaded.
     */
    private void initPanel(int panelLoaded) {
        loadingThumbnails(true);
        controller.loadList(panelLoaded);
        setHomePageTitle();
        panel.removeAll();
        panel.setPreferredSize(new Dimension(100, 100));
        spane.setViewportView(panel);
        Thread initPanelThread = new Thread() {
            @Override
            public void run() {
                int x = 5, y = 5;
                int numOfThumbnails = controller.getListLength();
                JLabel[] thumbnailLbl = new JLabel[numOfThumbnails];
                JLabel[] thumbnailTitleLbl = new JLabel[numOfThumbnails];;
                for (int i = 0; i <= thumbnailLbl.length - 1; i++) {

                    final int position = i;
                    final Picture picture = controller.getPictureFromList(position);
                    final Album album = controller.getAlbumFromList(position);
                    final JPopupMenu popup = createPopupMenu(position, picture, album);

                    thumbnailLbl[i] = createThumbnailLbl(x, y, position, album, popup);
                    panel.add(thumbnailLbl[i]);

                    thumbnailTitleLbl[i] = createThumbnailTitleLbl(x, y, picture, album);
                    panel.add(thumbnailTitleLbl[i]);

                    if (i < thumbnailLbl.length - 1) {
                        int x_tmp = x + controller.getThumbnailDimensions() + 5;
                        if (x_tmp <= panel.getWidth() - (controller.getThumbnailDimensions() + 5)) {
                            x = x_tmp;
                        } else {
                            x = 5;
                            y = y + controller.getThumbnailDimensions() + 25;
                        }
                    }
                    panel.setPreferredSize(new Dimension(1260, y + controller.getThumbnailDimensions() + 25));
                    spane.setViewportView(panel);
                }
                loadingThumbnails(false);
            }
        };
        initPanelThread.start();
    }

    /**
     * TODO: desc.
     *
     * @param x
     * @param y
     * @param position
     * @param album
     * @param popup
     * @return
     */
    private JLabel createThumbnailLbl(int x, int y, int position,
            Album album, JPopupMenu popup) {
        JLabel thumbnailLbl = new JLabel();
        thumbnailLbl.setBounds(x, y,
                controller.getThumbnailDimensions(),
                controller.getThumbnailDimensions());
        thumbnailLbl.setIcon(controller.buildThumbnail(position));
        thumbnailLbl.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    switch (controller.getPanelLoaded()) {
                        case Controller.LOADED_LOCAL_PICTURES:
                        case Controller.LOADED_LOCAL_ALBUM_PICTURES:
                            controller.showPictureViewFrame(position);
                            break;
                        case Controller.LOADED_LOCAL_ALBUMS:
                            controller.setSelectedAlbum(album);
                            initPanel(Controller.LOADED_LOCAL_ALBUM_PICTURES);
                        default:
                            break;
                    }
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    popup.show(e.getComponent(),
                            e.getX(), e.getY());
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setCursor(Cursor.getDefaultCursor());
            }
        });
        return thumbnailLbl;
    }

    /**
     * TODO: desc.
     *
     * @param x
     * @param y
     * @param picture
     * @param album
     * @return
     */
    private JLabel createThumbnailTitleLbl(int x, int y, Picture picture, Album album) {
        JLabel thumbnailTitleLbl = new JLabel();
        thumbnailTitleLbl.setBounds(x, y + controller.getThumbnailDimensions() + 2,
                controller.getThumbnailDimensions(), 20);
        switch (controller.getPanelLoaded()) {
            case Controller.LOADED_LOCAL_PICTURES:
            case Controller.LOADED_LOCAL_ALBUM_PICTURES:
                thumbnailTitleLbl.setText(picture.getTitle());
                break;
            case Controller.LOADED_LOCAL_ALBUMS:
                thumbnailTitleLbl.setText(album.getTitle().replaceAll(".txt", ""));
                break;
            default:
                break;
        }
        return thumbnailTitleLbl;
    }

    /**
     * TODO: desc
     *
     * @param picture
     * @param album
     * @return
     */
    private JPopupMenu createPopupMenu(int position, Picture picture, Album album) {
        switch (controller.getPanelLoaded()) {
            case Controller.LOADED_LOCAL_PICTURES:
            case Controller.LOADED_LOCAL_ALBUM_PICTURES:
            case Controller.LOADED_ONLINE_PICTURES:
            case Controller.LOADED_ONLINE_ALBUM_PICTURES:
                return setPicturePopup(position, picture);
            case Controller.LOADED_LOCAL_ALBUMS:
            case Controller.LOADED_ONLINE_ALBUMS:
                return setAlbumPopup(position, album);
            default:
                return null;
        }
    }

    /**
     * Creates the popup menu to be displayed when user is right cliking on a
     * picture.
     *
     * @param picture
     * @return popup menu
     */
    private JPopupMenu setPicturePopup(int position, Picture picture) {
        JPopupMenu popup = new JPopupMenu();
        JMenuItem showPicInfo = new JMenuItem("Information");
        showPicInfo.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                new InfoView(picture).setVisible(true);
            }
        });
        popup.add(showPicInfo);
        if (controller.getPanelLoaded() == Controller.LOADED_LOCAL_ALBUM_PICTURES) {
            JMenuItem removePicFromAlbum = new JMenuItem("Remove from Album");
            removePicFromAlbum.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    controller.removePictureFromAlbum(position);
                    initPanel(Controller.LOADED_LOCAL_ALBUM_PICTURES);
                }
            });
            popup.add(removePicFromAlbum);
        }
        return popup;
    }

    /**
     * Creates the popup menu to be displayed when user is right cliking on an
     * album.
     *
     * @param album
     * @return popup menu
     */
    private JPopupMenu setAlbumPopup(int position, Album album) {
        JPopupMenu popup = new JPopupMenu();
        JMenuItem showAlbumInfo = new JMenuItem("Information");
        showAlbumInfo.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                new InfoView(album).setVisible(true);
            }
        });
        popup.add(showAlbumInfo);
        JMenuItem deleteAlbum = new JMenuItem("Delete Album");
        deleteAlbum.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                controller.eraseAlbum(position);
                initPanel(Controller.LOADED_LOCAL_ALBUMS);
            }
        });
        popup.add(deleteAlbum);
        return popup;
    }

    /**
     * TODO: desc.
     *
     * @param flag is supposed to be true when user logs in, or otherwise
     */
    private void connectingUser(boolean flag) {
        signinItem.setVisible(!flag);
        signupItem.setVisible(!flag);
        logoutItem.setVisible(flag);
        uploadPicturesItem.setVisible(flag);
        createOnlineAlbum.setEnabled(flag);
        showOnlinePictures.setEnabled(flag);
        showOnlineAlbums.setEnabled(flag);
        accountMenu.setText(controller.getUser().getUsername());
    }

    /**
     * TODO: desc.
     *
     * @param flag
     */
    private void loadingThumbnails(boolean flag) {
        home.setEnabled(!flag);
        logoutItem.setEnabled(!flag);
        defaultFolder.setEnabled(!flag);
        selectFolder.setEnabled(!flag);
        showAlbums.setEnabled(!flag);
        if (controller.getUser().isLogedIn()) {
            showOnlinePictures.setEnabled(!flag);
            showOnlineAlbums.setEnabled(!flag);
        }
    }
    
    /**
     * TODO: desc.
     */
    private void setHomePageTitle() {
        this.setTitle(this.getTitle().split(", Current Directory: ")[0]
                + ", Current Directory: "
                + controller.getSelectedFolder());
    }

    /**
     * Display a window that lets user select a folder.
     */
    public void chooseSelectedFolder() {
        JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setCurrentDirectory(new File(controller.getSelectedFolder()));
        controller.setFcResult(fc.showDialog(null, "Select"));
        if (fc.getSelectedFile() != null) {
            controller.setSelectedFolder(fc.getSelectedFile().toString());
        }
    }

    /**
     * Display a window that lets user select files.
     */
    public void chooseFiles() {
        JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setMultiSelectionEnabled(true);
        fc.setCurrentDirectory(new File(new SettingsRepository(TAG_DEFAULT_FOLDER, "").getSetting()));
        controller.setFcResult(fc.showDialog(null, "Select"));
        if (controller.getFcResult() != JFileChooser.CANCEL_OPTION) {
            controller.setSelectedFiles(Arrays.asList(fc.getSelectedFiles()));
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        spane = new javax.swing.JScrollPane();
        panel = new javax.swing.JPanel();
        menubar = new javax.swing.JMenuBar();
        folderMenu = new javax.swing.JMenu();
        home = new javax.swing.JMenuItem();
        showOnlinePictures = new javax.swing.JMenuItem();
        selectFolder = new javax.swing.JMenuItem();
        defaultFolder = new javax.swing.JMenuItem();
        albumsMenu = new javax.swing.JMenu();
        createAlbum = new javax.swing.JMenuItem();
        showAlbums = new javax.swing.JMenuItem();
        sep1 = new javax.swing.JPopupMenu.Separator();
        createOnlineAlbum = new javax.swing.JMenuItem();
        showOnlineAlbums = new javax.swing.JMenuItem();
        accountMenu = new javax.swing.JMenu();
        signinItem = new javax.swing.JMenuItem();
        signupItem = new javax.swing.JMenuItem();
        uploadPicturesItem = new javax.swing.JMenuItem();
        logoutItem = new javax.swing.JMenuItem();
        sep2 = new javax.swing.JPopupMenu.Separator();
        settingsItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Project_MP_2018");
        setBounds(new java.awt.Rectangle(0, 0, 1280, 720));
        setLocation(new java.awt.Point(0, 0));
        setMaximizedBounds(new java.awt.Rectangle(0, 0, 1280, 720));
        setMaximumSize(new java.awt.Dimension(1280, 720));
        setMinimumSize(new java.awt.Dimension(1280, 720));
        setName("frame"); // NOI18N
        setResizable(false);
        setSize(new java.awt.Dimension(1280, 720));

        spane.setAutoscrolls(true);
        spane.setHorizontalScrollBar(null);
        spane.setMinimumSize(new java.awt.Dimension(100, 100));
        spane.setPreferredSize(new java.awt.Dimension(100, 100));

        panel.setAutoscrolls(true);
        panel.setPreferredSize(new java.awt.Dimension(100, 100));

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1278, Short.MAX_VALUE)
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 697, Short.MAX_VALUE)
        );

        spane.setViewportView(panel);

        menubar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        folderMenu.setText("Folder");

        home.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_HOME, 0));
        home.setText("Home");
        home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeActionPerformed(evt);
            }
        });
        folderMenu.add(home);

        showOnlinePictures.setText("Show Online Pictures");
        showOnlinePictures.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showOnlinePicturesActionPerformed(evt);
            }
        });
        folderMenu.add(showOnlinePictures);

        selectFolder.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_MASK));
        selectFolder.setText("Change Folder");
        selectFolder.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        selectFolder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectFolderActionPerformed(evt);
            }
        });
        folderMenu.add(selectFolder);

        defaultFolder.setText("Default Folder");
        defaultFolder.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        defaultFolder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                defaultFolderActionPerformed(evt);
            }
        });
        folderMenu.add(defaultFolder);

        menubar.add(folderMenu);

        albumsMenu.setText("Albums");

        createAlbum.setText("Create Album");
        createAlbum.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        createAlbum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createAlbumActionPerformed(evt);
            }
        });
        albumsMenu.add(createAlbum);

        showAlbums.setText("Show Albums");
        showAlbums.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        showAlbums.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showAlbumsActionPerformed(evt);
            }
        });
        albumsMenu.add(showAlbums);
        albumsMenu.add(sep1);

        createOnlineAlbum.setText("Create Online Album");
        createOnlineAlbum.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        albumsMenu.add(createOnlineAlbum);

        showOnlineAlbums.setText("Show Online Albums");
        showOnlineAlbums.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        albumsMenu.add(showOnlineAlbums);

        menubar.add(albumsMenu);

        signinItem.setText("Sign In");
        signinItem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        signinItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signinItemActionPerformed(evt);
            }
        });
        accountMenu.add(signinItem);

        signupItem.setText("Sign Up");
        signupItem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        signupItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signupItemActionPerformed(evt);
            }
        });
        accountMenu.add(signupItem);

        uploadPicturesItem.setText("Upload");
        uploadPicturesItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uploadPicturesItemActionPerformed(evt);
            }
        });
        accountMenu.add(uploadPicturesItem);

        logoutItem.setText("Log Out");
        logoutItem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        logoutItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutItemActionPerformed(evt);
            }
        });
        accountMenu.add(logoutItem);
        accountMenu.add(sep2);

        settingsItem.setText("Settings");
        settingsItem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        accountMenu.add(settingsItem);

        menubar.add(accountMenu);

        setJMenuBar(menubar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spane, javax.swing.GroupLayout.DEFAULT_SIZE, 1280, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 699, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // <editor-fold defaultstate="collapsed" desc="buttons">
    private void defaultFolderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_defaultFolderActionPerformed
        chooseSelectedFolder();
        if (controller.getFcResult() != JFileChooser.CANCEL_OPTION) {
            controller.changeDefaultFolder();
            initPanel(Controller.LOADED_LOCAL_PICTURES);
        }
    }//GEN-LAST:event_defaultFolderActionPerformed

    private void selectFolderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectFolderActionPerformed
        chooseSelectedFolder();
        if (controller.getFcResult() != JFileChooser.CANCEL_OPTION) {
            initPanel(Controller.LOADED_LOCAL_PICTURES);
        }
    }//GEN-LAST:event_selectFolderActionPerformed

    private void createAlbumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createAlbumActionPerformed
        AlbumCreator ac = new AlbumCreator(this, controller);
        ac.setVisible(true);
        controller.createAlbum(ac);
    }//GEN-LAST:event_createAlbumActionPerformed

    private void showAlbumsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showAlbumsActionPerformed
        initPanel(Controller.LOADED_LOCAL_ALBUMS);
    }//GEN-LAST:event_showAlbumsActionPerformed

    private void signinItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signinItemActionPerformed
        SignIn signInDialog = new SignIn(this);
        signInDialog.setVisible(true);
        boolean logedIn = controller.signinRoutine(signInDialog.username, signInDialog.password);
        if (!logedIn) {
            JOptionPane.showMessageDialog(signInDialog, "Given invalid information.");
        }
        connectingUser(logedIn);
    }//GEN-LAST:event_signinItemActionPerformed

    private void signupItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signupItemActionPerformed
        SignUp signUpDialog = new SignUp(this);
        signUpDialog.setVisible(true);
        if (signUpDialog.status == SignUp.SUBMIT) {
            User user = new User(0,
                    signUpDialog.getUsername(),
                    signUpDialog.getEmail());
            String error = controller.signupRoutine(user,
                    String.valueOf(signUpDialog.getPassword()));
            if (error.equals("")) {
                JOptionPane.showMessageDialog(this, "Sign Up Completed.");
            } else {
                JOptionPane.showMessageDialog(this, error);
            }
        }
    }//GEN-LAST:event_signupItemActionPerformed

    private void logoutItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutItemActionPerformed
        if (controller.signoutRoutine()) {
            initPanel(Controller.LOADED_LOCAL_PICTURES);
        }
        connectingUser(controller.getUser().isLogedIn());
    }//GEN-LAST:event_logoutItemActionPerformed

    private void homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeActionPerformed
        controller.gotoDefaultDestination();
        initPanel(Controller.LOADED_LOCAL_PICTURES);
    }//GEN-LAST:event_homeActionPerformed

    private void uploadPicturesItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uploadPicturesItemActionPerformed
        chooseFiles();
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        controller.uploadPictures();
        setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_uploadPicturesItemActionPerformed

    private void showOnlinePicturesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showOnlinePicturesActionPerformed
        initPanel(Controller.LOADED_ONLINE_PICTURES);
    }//GEN-LAST:event_showOnlinePicturesActionPerformed
    // </editor-fold>

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu accountMenu;
    private javax.swing.JMenu albumsMenu;
    private javax.swing.JMenuItem createAlbum;
    private javax.swing.JMenuItem createOnlineAlbum;
    private javax.swing.JMenuItem defaultFolder;
    private javax.swing.JMenu folderMenu;
    private javax.swing.JMenuItem home;
    private javax.swing.JMenuItem logoutItem;
    private javax.swing.JMenuBar menubar;
    private javax.swing.JPanel panel;
    private javax.swing.JMenuItem selectFolder;
    private javax.swing.JPopupMenu.Separator sep1;
    private javax.swing.JPopupMenu.Separator sep2;
    private javax.swing.JMenuItem settingsItem;
    private javax.swing.JMenuItem showAlbums;
    private javax.swing.JMenuItem showOnlineAlbums;
    private javax.swing.JMenuItem showOnlinePictures;
    private javax.swing.JMenuItem signinItem;
    private javax.swing.JMenuItem signupItem;
    private javax.swing.JScrollPane spane;
    private javax.swing.JMenuItem uploadPicturesItem;
    // End of variables declaration//GEN-END:variables
}
