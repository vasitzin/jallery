package ui;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import model.Album;
import model.Picture;

/**
 *
 * @author VaseiliosTzinis
 */
public class InfoView extends javax.swing.JDialog {

    Picture picture;
    Album album;

    /**
     * Creates new form InfoView
     *
     * @param picture
     */
    public InfoView(Picture picture) {
        this.picture = picture;
        initComponents();
        this.setLocationRelativeTo(null);
        initPanelByPicture();
    }

    /**
     * Creates new form InfoView
     *
     * @param album
     */
    public InfoView(Album album) {
        this.album = album;
        initComponents();
        this.setLocationRelativeTo(null);
        spane.getVerticalScrollBar().setUnitIncrement(33);
        initPanelByAlbum();
    }

    /**
     * Creates labels with picture's information for the panel and adds them to
     * it.
     */
    private void initPanelByPicture() {
        int i = 0;
        int x = 5, y = 5;

        JLabel[] tags = new JLabel[Picture.TOTAL_NUM_TAGS];

        for (JLabel tag : tags) {
            tags[i] = new JLabel();
            tags[i].setBounds(x, y, 270, 25);

            i++;
            y = y + 30;
        }

        try {
            tags[0].setText(picture.getTitle());
        } catch (NullPointerException ex) {
            tags[0].setText("TITLE" + ex);
        }
        try {
            tags[1].setText(picture.getDate().toString());
        } catch (NullPointerException ex) {
            tags[1].setText("DATE " + ex);
        }
        try {
            if (picture.getLat() != -1 || picture.getLongt() != -1) {
                tags[2].setText(String.valueOf(picture.getLat()) + " - " +
                        String.valueOf(picture.getLongt()));
                tags[2].addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (SwingUtilities.isLeftMouseButton(e)) {
                            new GeolocationMap(picture.getLat(), picture.getLongt()).setVisible(true);
                        }
                    }
                });
            }
            else {
                throw null;
            }
        } catch (NullPointerException ex) {
            tags[2].setText("GEOTAG " + ex);
        }
        try {
            tags[3].setText((int) picture.getDimension().getWidth() + " * " +
                    (int) picture.getDimension().getHeight());
        } catch (NullPointerException ex) {
            tags[3].setText("DIMENSION " + ex);
        }
        try {
            tags[4].setText(picture.getSize() + " bytes");
        } catch (NullPointerException ex) {
            tags[4].setText("TOTAL_BYTES " + ex);
        }

        panel.setPreferredSize(new Dimension(270, y + 30));
        for (JLabel tag : tags) {
            panel.add(tag);
        }
        spane.setViewportView(panel);
    }

    /**
     * Creates labels with album's information for the panel and adds them to
     * it.
     */
    private void initPanelByAlbum() {
        int i = 0;
        int x = 5, y = 5;

        JLabel[] tags = new JLabel[Album.TOTAL_NUM_TAGS];

        for (JLabel tag : tags) {
            tags[i] = new JLabel();
            tags[i].setBounds(x, y, 270, 25);

            i++;
            y = y + 30;
        }

        try {
            tags[0].setText(album.getTitle());
        } catch (NullPointerException ex) {
            tags[0].setText("Title not found!");
        }
        try {
            tags[1].setText(album.getDate().toString());
        } catch (NullPointerException ex) {
            tags[1].setText("Date not found!");
        }
        try {
            tags[2].setText("Contains " + album.getPictures().size() + " images");
        } catch (NullPointerException ex) {
            tags[2].setText("Number of images not found!\n" + ex);
        }

        panel.setPreferredSize(new Dimension(270, y + 30));
        for (JLabel tag : tags) {
            panel.add(tag);
        }
        spane.setViewportView(panel);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        spane = new javax.swing.JScrollPane();
        panel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Metadata");
        setMaximumSize(new java.awt.Dimension(300, 600));
        setMinimumSize(new java.awt.Dimension(300, 600));
        setPreferredSize(new java.awt.Dimension(300, 600));
        setResizable(false);

        spane.setMaximumSize(new java.awt.Dimension(100, 100));
        spane.setMinimumSize(new java.awt.Dimension(100, 100));

        panel.setMaximumSize(new java.awt.Dimension(100, 100));
        panel.setMinimumSize(new java.awt.Dimension(100, 100));
        panel.setPreferredSize(new java.awt.Dimension(100, 100));

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 298, Short.MAX_VALUE)
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 598, Short.MAX_VALUE)
        );

        spane.setViewportView(panel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel panel;
    private javax.swing.JScrollPane spane;
    // End of variables declaration//GEN-END:variables
}
