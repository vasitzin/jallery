package ui;

import geolocation.Geolocation;
import javafx.embed.swing.JFXPanel;

/**
 *
 * @author VaseiliosTzinis
 */
public class GeolocationMap extends javax.swing.JDialog {
    
    JFXPanel panel;
    String address;

    /**
     * Creates new form GeolocationMap
     *
     * @param lat
     * @param longt
     */
    public GeolocationMap(double lat, double longt) {
        initComponents();
        this.setLocationRelativeTo(null);
        Geolocation geoloc = new Geolocation(lat, longt);
        panel = geoloc.getJFXPanel();
        this.getContentPane().add(panel);
        this.pack();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(720, 720));
        setMinimumSize(new java.awt.Dimension(720, 720));
        setResizable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 720, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 720, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
