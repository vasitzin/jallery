package ui;

import controller.Controller;
import java.awt.Frame;
import repository.SettingsRepository;
import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JFileChooser;
import repository.picture.PictureRepository;

/**
 *
 * @author VaseiliosTzinis
 */
public class AlbumCreator extends javax.swing.JDialog {

    PictureRepository picRep = new PictureRepository();
    public static final int MANUALLY = 1;
    public static final int AUTO_DATE = 2;

    Controller controller;
    int selectedOption = AlbumCreator.MANUALLY;
    String albumName = null;
    LocalDate selectedLocalDate = null;
    float[] selectedLatlong = null;
    JFileChooser fc = new JFileChooser();
    List<File> selectedFiles = new ArrayList<>();
    List<File> selectedDirectories = new ArrayList<>();

    /**
     * Creates new form AlbumCreator.
     *
     * @param parent
     * @param controller
     */
    public AlbumCreator(Frame parent, Controller controller) {
        super(parent, true);
        initComponents();
        this.controller = controller;
        this.setLocationRelativeTo(null);
    }

    public int getSelectedOption() {
        return selectedOption;
    }

    public String getSelectedName() {
        return albumName;
    }

    public List<File> getSelectedFiles() {
        return selectedFiles;
    }

    public LocalDate getSelectedLocalDate() {
        return selectedLocalDate;
    }

    public float[] getSelectedLatlong() {
        return selectedLatlong;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtnGroup = new javax.swing.ButtonGroup();
        albumNameTf = new javax.swing.JTextField();
        choosePictureManuallyBtn = new javax.swing.JButton();
        submitBtn = new javax.swing.JButton();
        albumNameLbl = new javax.swing.JLabel();
        chooseFolderAutoByDateBtn = new javax.swing.JButton();
        manuallyRbtn = new javax.swing.JRadioButton();
        autoByDateRbtn = new javax.swing.JRadioButton();
        datePicker = new com.github.lgooddatepicker.components.DatePicker();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(289, 179));
        setMinimumSize(new java.awt.Dimension(289, 179));
        setResizable(false);

        choosePictureManuallyBtn.setText("Add Images");
        choosePictureManuallyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choosePictureManuallyBtnActionPerformed(evt);
            }
        });

        submitBtn.setText("SUBMIT");
        submitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitBtnActionPerformed(evt);
            }
        });

        albumNameLbl.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        albumNameLbl.setText("Album Name:");

        chooseFolderAutoByDateBtn.setText("Choose Folder");
        chooseFolderAutoByDateBtn.setEnabled(false);
        chooseFolderAutoByDateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseFolderAutoByDateBtnActionPerformed(evt);
            }
        });

        BtnGroup.add(manuallyRbtn);
        manuallyRbtn.setSelected(true);
        manuallyRbtn.setText("Select from Folder");
        manuallyRbtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        manuallyRbtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        manuallyRbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manuallyRbtnActionPerformed(evt);
            }
        });

        BtnGroup.add(autoByDateRbtn);
        autoByDateRbtn.setText("Select by Date");
        autoByDateRbtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        autoByDateRbtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        autoByDateRbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoByDateRbtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(submitBtn)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(manuallyRbtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(autoByDateRbtn)
                            .addComponent(albumNameLbl, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(datePicker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(choosePictureManuallyBtn)
                                    .addComponent(chooseFolderAutoByDateBtn))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(albumNameTf))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(albumNameTf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(albumNameLbl))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(manuallyRbtn)
                    .addComponent(choosePictureManuallyBtn))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(autoByDateRbtn)
                    .addComponent(datePicker, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chooseFolderAutoByDateBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addComponent(submitBtn)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // <editor-fold defaultstate="collapsed" desc="buttons">
    private void submitBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitBtnActionPerformed
        if (!albumNameTf.getText().equals("")) {
            if (!selectedFiles.isEmpty()) {
                albumName = albumNameTf.getText();
                selectedLocalDate = datePicker.getDate();
                selectedLatlong = selectedLatlong;  //TODO latlong assignment
                this.dispose();
            } else {
                //TODO warning no selected files
            }
        } else {
            //TODO warning no name
        }
    }//GEN-LAST:event_submitBtnActionPerformed

    private void manuallyRbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manuallyRbtnActionPerformed
        choosePictureManuallyBtn.setEnabled(true);
        chooseFolderAutoByDateBtn.setEnabled(false);
        datePicker.setEnabled(false);
        selectedFiles.clear();
        selectedLocalDate = null;
        selectedLatlong = null;
        selectedOption = AlbumCreator.MANUALLY;
    }//GEN-LAST:event_manuallyRbtnActionPerformed

    private void choosePictureManuallyBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choosePictureManuallyBtnActionPerformed
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setFileFilter(PictureRepository.PICTURE_TYPE);
        fc.setMultiSelectionEnabled(true);
        fc.setCurrentDirectory(new File(new SettingsRepository(Controller.TAG_DEFAULT_FOLDER, "").getSetting()));
        int fcResult = fc.showDialog(null, "Select");
        if (fcResult != JFileChooser.CANCEL_OPTION) {
            selectedFiles.addAll(Arrays.asList(fc.getSelectedFiles()));
        }
    }//GEN-LAST:event_choosePictureManuallyBtnActionPerformed

    private void autoByDateRbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoByDateRbtnActionPerformed
        choosePictureManuallyBtn.setEnabled(false);
        chooseFolderAutoByDateBtn.setEnabled(true);
        datePicker.setEnabled(true);
        selectedFiles.clear();
        selectedLatlong = null;
        selectedOption = AlbumCreator.AUTO_DATE;
    }//GEN-LAST:event_autoByDateRbtnActionPerformed

    private void chooseFolderAutoByDateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseFolderAutoByDateBtnActionPerformed
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setMultiSelectionEnabled(true);
        fc.setCurrentDirectory(new File(new SettingsRepository(Controller.TAG_DEFAULT_FOLDER, "").getSetting()));
        int fcResult = fc.showDialog(null, "Select");
        if (fcResult != JFileChooser.CANCEL_OPTION) {
            selectedDirectories.addAll(Arrays.asList(fc.getSelectedFiles()));
            selectedDirectories.forEach((dir) -> {
                selectedFiles.addAll(Arrays.asList(dir.listFiles()));
            });
        }
    }//GEN-LAST:event_chooseFolderAutoByDateBtnActionPerformed
    // </editor-fold>

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup BtnGroup;
    private javax.swing.JLabel albumNameLbl;
    private javax.swing.JTextField albumNameTf;
    private javax.swing.JRadioButton autoByDateRbtn;
    private javax.swing.JButton chooseFolderAutoByDateBtn;
    private javax.swing.JButton choosePictureManuallyBtn;
    private com.github.lgooddatepicker.components.DatePicker datePicker;
    private javax.swing.JRadioButton manuallyRbtn;
    private javax.swing.JButton submitBtn;
    // End of variables declaration//GEN-END:variables
}
