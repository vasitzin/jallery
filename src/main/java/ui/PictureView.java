package ui;

import controller.Controller;
import java.awt.Dimension;
import java.awt.event.MouseWheelEvent;
import java.math.BigInteger;
import model.Picture;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import repository.picture.PictureRepository;

/**
 *
 * @author VaseiliosTzinis
 */
public class PictureView extends javax.swing.JFrame {

    PictureRepository picRep = new PictureRepository();
    Picture picture;
    int zoomWidth;
    int zoomHeight;
    int gcd;
    JLabel label = new JLabel();

    public PictureView() {
    }

    /**
     * Creates new form PictureView
     *
     * @param positionOfPicture
     * @param controller
     */
    public PictureView(int positionOfPicture, Controller controller) {
        picture = controller.getPictureFromList(positionOfPicture);
        ImageIcon icon = controller.buildPicture(positionOfPicture);

        initComponents();
        this.setLocationRelativeTo(null);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setTitle(picture.getTitle());
        spane.getVerticalScrollBar().setUnitIncrement(33);

        zoomWidth = picture.getDimension().width;
        zoomHeight = picture.getDimension().height;
        gcd = GCD(zoomWidth, zoomHeight);

        label.setSize(picture.getDimension().width, picture.getDimension().height);
        label.setIcon(icon);

        label.addMouseWheelListener((MouseWheelEvent e) -> {
            panel.removeAll();
            int zoomWidthTmp = zoomWidth;
            int zoomHeightTmp = zoomHeight;

            zoomWidth = zoomWidth - ((picture.getDimension().width / gcd) * e.getWheelRotation());
            zoomHeight = zoomHeight - ((picture.getDimension().height / gcd) * e.getWheelRotation());
            if (zoomWidth <= 0 || zoomHeight <= 0) {
                zoomWidth = zoomWidthTmp;
                zoomHeight = zoomHeightTmp;
            }

            label.setSize(zoomWidth, zoomHeight);
            label.setIcon(new ImageIcon(icon
                    .getImage()
                    .getScaledInstance(zoomWidth, zoomHeight, java.awt.Image.SCALE_SMOOTH)));
            panel.setPreferredSize(new Dimension(zoomWidth, zoomHeight));
            panel.add(label);
            spane.setViewportView(panel);
        });

        panel.setPreferredSize(new Dimension(picture.getDimension().width, picture.getDimension().height));
        panel.add(label);
        spane.setViewportView(panel);
    }

    private static int GCD(int width, int height) {
        BigInteger heightTmp1 = BigInteger.valueOf(width);
        BigInteger heightTmp2 = BigInteger.valueOf(height);
        BigInteger gcd = heightTmp1.gcd(heightTmp2);
        
        if (gcd.intValue() < 10) {
            gcd = BigInteger.valueOf(10);
        }
        else if (gcd.intValue() > 100) {
            gcd = BigInteger.valueOf(100);
        }
        
        return gcd.intValue();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        spane = new javax.swing.JScrollPane();
        panel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(100, 100));
        setSize(new java.awt.Dimension(100, 100));

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 398, Short.MAX_VALUE)
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 298, Short.MAX_VALUE)
        );

        spane.setViewportView(panel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spane)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel panel;
    private javax.swing.JScrollPane spane;
    // End of variables declaration//GEN-END:variables
}
