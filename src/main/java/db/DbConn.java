package db;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import java.util.List;
import org.bson.Document;

/**
 *
 * @author VaseiliosTzinis
 */
public class DbConn {

    public static final String USERS_COLLECTION_TAG = "users";
    public static final String PICTURES_COLLECTION_TAG = "pictures.files";
    public static final String ALBUMS_COLLECTION_TAG = "albums";

    String uri = "mongodb://vasitzin:70_PmP@ds117590.mlab.com:17590/project_mp";
    MongoClient mongoClient;
    MongoDatabase mongoDb;
    MongoCollection<Document> mongoCollection;

    public DbConn() {
        mongoClient = MongoClients.create(uri);
        mongoDb = mongoClient.getDatabase("project_mp");
    }

    public void insertOneInMongoCollection(Document document) {
        mongoCollection.insertOne(document);
    }

    public void insertManyInMongoCollection(List<Document> documents) {
        mongoCollection.insertMany(documents);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    public String getUri() {
        return uri;
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }

    public MongoDatabase getMongoDb() {
        return mongoDb;
    }

    public MongoCollection<Document> getMongoCollection() {
        return mongoCollection;
    }

    public void setMongoCollection(String collection) {
        this.mongoCollection = mongoDb.getCollection(collection);
    }
    // </editor-fold>
}
