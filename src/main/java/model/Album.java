package model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author VaseiliosTzinis
 */
public class Album {

    /**
     * Number of variables the object has.
     */
    public static final int TOTAL_NUM_TAGS = 3;

    String title;
    List<Picture> pictures;
    Date date;

    /**
     * Full Constructor.
     *
     * @param title
     * @param pictures
     * @param date
     */
    public Album(String title, List<Picture> pictures, Date date) {
        this.title = title;
        this.pictures = pictures;
        this.date = date;
    }

    // <editor-fold defaultstate="collapsed" desc="Getters'n'Setters">
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    // </editor-fold>
}
