package model;

/**
 *
 * @author VaseiliosTzinis
 */
public class User {

    int id;
    String username;
    String email;
    boolean logedIn;

    /**
     * Empty Constructor.
     */
    public User() {
        this.username = "Guest";
        this.logedIn = false;
    }

    /**
     * Full Constructor.
     *
     * @param id
     * @param username
     * @param email
     */
    public User(int id, String username, String email) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.logedIn = true;
    }

    // <editor-fold defaultstate="collapsed" desc="Getters'n'Setters">
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isLogedIn() {
        return logedIn;
    }
    // </editor-fold>
}
