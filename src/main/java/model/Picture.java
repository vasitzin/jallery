package model;

import java.awt.Dimension;
import java.util.Date;

/**
 *
 * @author VaseiliosTzinis
 */
public class Picture {

    /**
     * Number of variables the object has.
     */
    public static final int TOTAL_NUM_TAGS = 5;

    String title;
    Dimension dimension;
    Date date;
    double lat;
    double longt;
    int size;

    /**
     * Full Constructor.
     *
     * @param title the name of the image
     * @param dimension
     * @param date
     * @param lat
     * @param longt
     * @param size
     */
    public Picture(String title, Dimension dimension, Date date, double lat, double longt, int size) {
        this.title = title;
        this.dimension = dimension;
        this.date = date;
        this.lat = lat;
        this.longt = longt;
        this.size = size;
    }

    // <editor-fold defaultstate="collapsed" desc="Getters'n'Setters">
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLongt() {
        return longt;
    }

    public void setLongt(double longt) {
        this.longt = longt;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
    // </editor-fold>
}
