package repository;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.GridFSFindIterable;
import com.mongodb.client.gridfs.model.GridFSFile;
import static com.mongodb.client.model.Filters.eq;
import db.DbConn;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import model.User;
import org.bson.Document;

/**
 *
 * @author VaseiliosTzinis
 */
public class UserRepository {

    DbConn dbConn;
    User user;

    int count;

    /**
     *
     * @param dbConn
     * @param user
     */
    public UserRepository(DbConn dbConn, User user) {
        this.dbConn = dbConn;
        this.user = user;
    }

    /**
     *
     * @param password
     * @return
     */
    public User signInUser(String password) {

        dbConn.setMongoCollection(DbConn.USERS_COLLECTION_TAG);
        FindIterable<Document> documents = dbConn.getMongoCollection().find(
                new BasicDBObject("Username", user.getUsername())
                        .append("Password", password));

        MongoCursor<Document> cursor = documents.iterator();
        if (cursor.hasNext()) {
            for (Document document : documents) {
                User user = new User(document.getInteger("Id"),
                        document.getString("Username"),
                        document.getString("Email"));
                return user;
            }
        }
        cursor.close();
        return new User();
    }

    /**
     *
     * @param password
     * @return
     */
    public String signUpUser(String password) {

        String error = signUpCheck();
        if (error.equals("")) {
            dbConn.setMongoCollection(DbConn.USERS_COLLECTION_TAG);
            Document document = new Document("Id", generateRandomId())
                    .append("Username", user.getUsername())
                    .append("Password", password)
                    .append("Email", user.getEmail());
            dbConn.insertOneInMongoCollection(document);
        }
        return error;
    }

    /**
     *
     * @param path
     * @return
     */
    public boolean doesPathExist(String path) {
        GridFSBucket gfsBucket = GridFSBuckets.create(dbConn.getMongoDb());
        GridFSFindIterable documents = gfsBucket.find(eq("filename", path));
        MongoCursor<GridFSFile> cursor = documents.iterator();
        if (cursor.hasNext()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return
     */
    public String signUpCheck() {
        String error;
        error = emailPatternCheck(user.getEmail());
        return error;
    }

    /**
     *
     * @param email
     * @return
     */
    private String emailPatternCheck(String email) {
        String error = "";
        Pattern pat = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher mat = pat.matcher(email);
        boolean matFound = mat.matches();
        if (!matFound) {
            count++;
            error = error + "\n" + count + ") " + email + " doesn't look like an e-m@il.";
        }
        return error;
    }

    /**
     * Generate a random unused Id for the signing up user.
     */
    private int generateRandomId() {
        Random rand = new Random();
        int id = rand.nextInt(9999);
        if (!doesIdExist(id)) {
            return id;
        } else {
            return generateRandomId();
        }
    }

    /**
     *
     * @param id
     * @return
     */
    private boolean doesIdExist(int id) {
        dbConn.setMongoCollection(DbConn.USERS_COLLECTION_TAG);
        FindIterable<Document> documents = dbConn.getMongoCollection().find(new BasicDBObject("Id", id));
        MongoCursor<Document> cursor = documents.iterator();
        if (cursor.hasNext()) {
            return true;
        } else {
            return false;
        }
    }
}
