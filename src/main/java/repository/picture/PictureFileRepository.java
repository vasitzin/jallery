package repository.picture;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.lang.GeoLocation;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.GpsDirectory;
import model.Picture;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.imageio.ImageIO;

/**
 *
 *
 * @author VaseiliosTzinis
 */
public class PictureFileRepository implements PictureRepositoryStrategy {

    File file;
    String title;
    Dimension dimension;
    Date date;
    double lat;
    double longt;
    int size;

    public PictureFileRepository(File file) {
        this.file = file;
    }

    /**
     *
     * @return a list of Pictures
     */
    @Override
    public List<Picture> getAllPictures() {
        File[] files;
        List<Picture> pictureList = new ArrayList<>();
        if (file.isDirectory()) {
            files = file.listFiles();
        } else {
            return null;
        }
        for (File aFile : files) {
            file = aFile;
            Picture picture = getAPicture();
            if (picture != null) {
                pictureList.add(picture);
            }
        }
        return pictureList;
    }

    /**
     *
     * @return a Picture
     */
    @Override
    public Picture getAPicture() {
        Picture picture;
        if (isCorrectType(file)) {
            title = file.getName();
            extractMetadata();
            picture = new Picture(title,
                    dimension,
                    date,
                    lat,
                    longt,
                    size);
            return picture;
        } else {
            return null;
        }
    }

    /**
     *
     * @param file
     * @return
     */
    private boolean isCorrectType(File file) {
        return PictureRepository.PICTURE_TYPE.accept(file) && !file.isDirectory();
    }

    /**
     * Extracts the metadata of an image.
     *
     * @return Picture with Metadata
     */
    private void extractMetadata() {
        dimension = extractDimension();
        size = extractSize();
        try {
            Metadata pictureMetadata = ImageMetadataReader.readMetadata(file);
            date = extractDate(pictureMetadata);
            double[] latlong = extractGeotag(pictureMetadata);
            lat = latlong[0];
            longt = latlong[1];
        } catch (ImageProcessingException | IOException ex) {
            date = null;
            lat = -1;
            longt = -1;
        }
    }

    /**
     * Returns a picture's Dimensions.
     *
     * @return picture dimensions
     */
    private Dimension extractDimension() {
        try {
            Dimension dim = new Dimension();
            BufferedImage bufPic = ImageIO.read(file);
            dim.width = bufPic.getWidth();
            dim.height = bufPic.getHeight();
            return dim;
        } catch (IOException ex) {
            return null;
        }
    }

    /**
     * Returns a picture's Original Date.
     *
     * @return a date as Date
     */
    private Date extractDate(Metadata pictureMetadata) {
        Date date = new Date();
        ExifSubIFDDirectory directory = pictureMetadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
        if (directory != null) {
            date = directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
        }
        return date;
    }

    /**
     * Returns a picture's Geotag.
     */
    private double[] extractGeotag(Metadata pictureMetadata) {
        double[] latlong = {0, 0};
        GpsDirectory gpsDirectory = pictureMetadata.getFirstDirectoryOfType(GpsDirectory.class);
        try {
            GeoLocation pictureLocation = gpsDirectory.getGeoLocation();
            latlong[0] = pictureLocation.getLatitude();
            latlong[1] = pictureLocation.getLongitude();
        }
        catch (NullPointerException ex) {
            latlong[0] = -1;
            latlong[1] = -1;
        }
        return latlong;
    }

    /**
     * Returns a picture's Size.
     *
     * @return size as Integer
     */
    private int extractSize() {
        try {
            int size;
            BasicFileAttributes attributes = Files.readAttributes(file.toPath(),
                    BasicFileAttributes.class);
            size = (int) attributes.size();
            return size;
        } catch (IOException ex) {
            return -1;
        }
    }
}
