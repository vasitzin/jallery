package repository.picture;

import com.drew.metadata.Metadata;
import model.Picture;
import java.awt.Dimension;
import java.util.Date;
import java.util.List;
import model.User;

/**
 *
 *
 * @author VaseiliosTzinis
 */
public class PictureDbRepository implements PictureRepositoryStrategy {

    User user;
    String title;
    Dimension dimension;
    Date date;
    double lat;
    double longt;
    int size;

    public PictureDbRepository(User user) {
        this.user = user;
    }

    /**
     *
     * @return a list of Pictures
     */
    @Override
    public List<Picture> getAllPictures() {
        //TODO
        return null;
    }

    /**
     *
     * @return A Picture
     */
    @Override
    public Picture getAPicture() {
        //TODO
        return null;
    }

    /**
     * Extracts the metadata of an image.
     *
     * @return Picture with Metadata
     */
    private void extractMetadata() {
        //TODO
    }

    /**
     * Returns a picture's Dimensions.
     *
     * @return picture dimensions
     */
    private Dimension extractDimension() {
        //TODO
        return null;
    }

    /**
     * Returns a picture's Original Date.
     *
     * @return a date as Date
     */
    private Date extractDate(Metadata pictureMetadata) {
        //TODO
        return null;
    }

    /**
     * Returns a picture's Geotag.
     */
    private double[] extractGeotag(Metadata pictureMetadata) {
        //TODO
        return null;
    }

    /**
     * Returns a picture's Size.
     *
     * @return size as Integer
     */
    private int extractSize() {
        //TODO
        return -1;
    }
}
