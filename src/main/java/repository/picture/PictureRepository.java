package repository.picture;

import java.util.List;
import javax.swing.filechooser.FileNameExtensionFilter;
import model.Picture;

/**
 *
 * @author VaseiliosTzinis
 */
public class PictureRepository {

    public static final FileNameExtensionFilter PICTURE_TYPE
            = new FileNameExtensionFilter("Image Files", "png", "jpg", "jpeg");

    private PictureRepositoryStrategy picRepStr;

    public void setPictureRepository(PictureRepositoryStrategy picRepStr) {
        this.picRepStr = picRepStr;
    }

    public List<Picture> getAllPictures() {
        return picRepStr.getAllPictures();
    }

    public Picture getAPicture() {
        return picRepStr.getAPicture();
    }
}
