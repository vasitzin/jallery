package repository.picture;

import java.util.List;
import model.Picture;

/**
 *
 * @author VaseiliosTzinis
 */
public interface PictureRepositoryStrategy {

    public List<Picture> getAllPictures();

    public Picture getAPicture();
}
