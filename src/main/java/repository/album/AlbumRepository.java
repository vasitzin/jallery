package repository.album;

import java.util.List;
import javax.swing.filechooser.FileNameExtensionFilter;
import model.Album;

/**
 *
 * @author VaseiliosTzinis
 */
public class AlbumRepository {

    public static final FileNameExtensionFilter ALBUM_TYPE
            = new FileNameExtensionFilter("Text Files", "txt");

    private AlbumRepositoryStrategy albumRepStr;

    public void setAlbumRepository(AlbumRepositoryStrategy albumRepStr) {
        this.albumRepStr = albumRepStr;
    }

    public List<Album> getAllAlbums() {
        return albumRepStr.getAllAlbums();
    }

    public Album getAnAlbum() {
        return albumRepStr.getAnAlbum();
    }

    public String getThumbnailByAlbumId() {
        return albumRepStr.getThumbnailByAlbumId();
    }

    public String getPicturePath(int position) {
        return albumRepStr.getPicturePath(position);
    }

    public void createAlbum(List<String> pictures) {
        albumRepStr.createAlbum(pictures);
    }

    public boolean eraseAlbum() {
        return albumRepStr.eraseAlbum();
    }

    public boolean removePictureFromAlbum(String albumValue) {
        return albumRepStr.removePictureFromAlbum(albumValue);
    }
}
