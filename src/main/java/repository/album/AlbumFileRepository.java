package repository.album;

import model.Album;
import model.Picture;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import repository.picture.PictureFileRepository;
import repository.picture.PictureRepository;

/**
 *
 * @author VaseiliosTzinis
 */
public class AlbumFileRepository implements AlbumRepositoryStrategy {

    PictureRepository picRep = new PictureRepository();
    File file;
    String title;
    List<Picture> pictures;
    Date date;

    public AlbumFileRepository(File file) {
        this.file = file;
    }

    /**
     *
     * @return a list of Albums
     */
    @Override
    public List<Album> getAllAlbums() {
        File[] files;
        List<Album> albumList = new ArrayList<>();
        if (file.isDirectory()) {
            files = file.listFiles();
        } else {
            return null;
        }
        for (File aFile : files) {
            file = aFile;
            Album album = getAnAlbum();
            if (album != null) {
                albumList.add(album);
            }
        }
        return albumList;
    }

    /**
     *
     * @return A List of Albums.
     */
    @Override
    public Album getAnAlbum() {
        Album album;
        if (isCorrectType(file)) {
            title = file.getName();
            setInformation();
            album = new Album(
                    title,
                    pictures,
                    date);
            return album;
        } else {
            return null;
        }
    }

    /**
     *
     * @param file
     * @return
     */
    private boolean isCorrectType(File file) {
        return AlbumRepository.ALBUM_TYPE.accept(file) && !file.isDirectory();
    }

    /**
     * Extracts the information of an album and returns a new Album with them.
     */
    private void setInformation() {
        pictures = getPicturesByAlbumId();
        date = getDateByAlbumId();
    }

    /**
     * Reads the album txt file that is located in the given absolute albumPath
     * as a String.
     *
     * @return a list of Picture Objects
     */
    private List<Picture> getPicturesByAlbumId() {
        try (BufferedReader bufferedReader = new BufferedReader(
                new FileReader(file))) {
            List<Picture> pictures = new ArrayList<>();
            String currentLine;
            String previousLine;
            boolean start = false;
            previousLine = bufferedReader.readLine().replace("\ufeff", "");
            while ((currentLine = bufferedReader.readLine()) != null) {
                if (previousLine.equals("PATHS")) {
                    start = true;
                } else if (currentLine.equals("/PATHS")) {
                    break;
                }

                if (start) {
                    picRep.setPictureRepository(new PictureFileRepository(new File(currentLine)));
                    Picture pic = picRep.getAPicture();
                    pictures.add(pic);
                }
                previousLine = currentLine;
            }
            bufferedReader.close();
            return pictures;
        } catch (FileNotFoundException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    /**
     * Reads the album txt file's, that is located in the given absolute
     * albumPath, creation date.
     *
     * @return the album's creation date
     */
    private Date getDateByAlbumId() {
        try {
            Date date;
            BasicFileAttributes attributes = Files.readAttributes(file.toPath(),
                    BasicFileAttributes.class);
            date = new Date(attributes.creationTime().toMillis());
            return date;
        } catch (IOException ex) {
            return null;
        }
    }

    /**
     * Returns the albumPath of the first albumValue inside the album's txt file
     * which is used as a thumbnail.
     *
     * @return albumPath of the thumbnail
     */
    @Override
    public String getThumbnailByAlbumId() {
        try (BufferedReader bufferedReader = new BufferedReader(
                new FileReader(file))) {
            String thumbnail = null;
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.equals("PATHS")) {
                    thumbnail = bufferedReader.readLine();
                }
            }
            bufferedReader.close();
            return thumbnail;
        } catch (FileNotFoundException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    /**
     *
     * @param position
     * @return
     */
    @Override
    public String getPicturePath(int position) {
        try (BufferedReader bufferedReader = new BufferedReader(
                new FileReader(file))) {
            String picturePath = null;
            int count = 0;
            String currentLine;
            String previousLine;
            boolean start = false;
            previousLine = bufferedReader.readLine();
            while ((currentLine = bufferedReader.readLine()) != null) {
                if (previousLine.equals("PATHS")) {
                    start = !start;
                } else if (currentLine.equals("/PATHS")) {
                    break;
                }

                if (start && position == count) {
                    picturePath = currentLine;
                    break;
                } else if (start) {
                    count++;
                }
                previousLine = currentLine;
            }
            bufferedReader.close();
            return picturePath;
        } catch (FileNotFoundException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    /**
     * Creates an album txt file.
     *
     * @param paths
     */
    @Override
    public void createAlbum(List<String> paths) {
        List<String> fileContents = new ArrayList<>();
        try {
            file.createNewFile();
            fileContents.add("PATHS");
            paths.forEach((path) -> {
                fileContents.add(path);
            });
            fileContents.add("/PATHS");
            Files.write(file.toPath(), fileContents, Charset.forName("UTF-8"));
        } catch (IOException ex) {
            //TODO
        }
    }

    /**
     * Deletes the Album txt file from the face of the earth.
     *
     * @return a flag that indicates wether the action was succesful
     */
    @Override
    public boolean eraseAlbum() {
        Path path = file.toPath();
        try {
            Files.delete(path);
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    /**
     * Deletes the line from the album txt with the given value.
     *
     * @param albumValue
     * @return
     */
    @Override
    public boolean removePictureFromAlbum(String albumValue) {
        String line;
        String removingLine = "";
        StringBuilder inputBuilder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(
                new FileReader(file))) {
            while ((line = bufferedReader.readLine()) != null) {
                if (line.equals(albumValue)) {
                    removingLine = line + "\n";
                }
                inputBuilder.append(line);
                inputBuilder.append('\n');
            }
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            return false;
        } catch (IOException ex) {
            return false;
        }

        String inputStr = inputBuilder.toString();
        inputStr = inputStr.replace(removingLine, "");

        try {
            FileOutputStream output = new FileOutputStream(file);
            output.write(inputStr.getBytes());
            output.close();
        } catch (FileNotFoundException ex) {
            return false;
        } catch (IOException ex) {
            return false;
        }
        return true;
    }
}
