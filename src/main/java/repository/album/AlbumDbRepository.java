package repository.album;

import model.Album;
import model.Picture;
import java.util.Date;
import java.util.List;
import model.User;

/**
 *
 * @author VaseiliosTzinis
 */
public class AlbumDbRepository implements AlbumRepositoryStrategy {

    User user;
    String title;
    List<Picture> pictures;
    Date date;

    public AlbumDbRepository(User user) {
        this.user = user;
    }

    /**
     *
     * @return a list of Albums
     */
    @Override
    public List<Album> getAllAlbums() {
        //TODO
        return null;
    }

    /**
     *
     * @return A List of Albums.
     */
    @Override
    public Album getAnAlbum() {
        //TODO
        return null;
    }

    /**
     * Extracts the information of an album and returns a new Album with them.
     */
    private void setInformation() {
        //TODO
    }

    /**
     * Reads the album txt file that is located in the given absolute path as a
     * String.
     *
     * @return a list of Picture Objects
     */
    private List<Picture> getPicturesByAlbumId() {
        //TODO
        return null;
    }

    /**
     * Reads the album txt file's, that is located in the given absolute path,
     * creation date.
     *
     * @return the album's creation date
     */
    private Date getDateByAlbumId() {
        //TODO
        return null;
    }

    /**
     * Returns the path of the first albumValue inside the album's txt file
     * which is used as a thumbnail.
     *
     * @return path of the thumbnail
     */
    @Override
    public String getThumbnailByAlbumId() {
        //TODO
        return null;
    }

    /**
     *
     * @param position
     * @return
     */
    @Override
    public String getPicturePath(int position) {
        //TODO
        return null;
    }

    /**
     * Creates an album txt file.
     *
     * @param ids
     */
    @Override
    public void createAlbum(List<String> ids) {
        //TODO
    }

    /**
     * Deletes the Album txt file from the face of the earth.
     *
     * @return a flag that indicates wether the action was succesful
     */
    @Override
    public boolean eraseAlbum() {
        //TODO
        return false;
    }

    /**
     * Deletes the line from the album txt with the given value.
     *
     * @param albumValue
     * @return
     */
    @Override
    public boolean removePictureFromAlbum(String albumValue) {
        //TODO
        return false;
    }
}
