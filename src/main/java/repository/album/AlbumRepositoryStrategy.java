package repository.album;

import java.util.List;
import model.Album;

/**
 *
 * @author VaseiliosTzinis
 */
public interface AlbumRepositoryStrategy {

    public List<Album> getAllAlbums();

    public Album getAnAlbum();

    public String getThumbnailByAlbumId();

    public String getPicturePath(int position);

    public void createAlbum(List<String> pictures);

    public boolean eraseAlbum();

    public boolean removePictureFromAlbum(String albumValue);
}
