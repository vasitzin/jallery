package repository;

import controller.Controller;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author VaseiliosTzinis
 */
public class SettingsRepository {

    String file = Controller.VALUE_SETTINGS_LOC;
    String seperator = Controller.TAG_VALUE_SEPERATOR;

    String settingTag;
    String newSetting;

    public SettingsRepository() {
    }

    /**
     * Constructor. If used just to read the setting file give a null as second
     * String.
     *
     * @param settingTag
     * @param newSetting
     */
    public SettingsRepository(String settingTag, String newSetting) {
        this.settingTag = settingTag;
        this.newSetting = newSetting;
    }

    /**
     * Creates a Settings.txt file with default parameters.
     *
     * @param settingsFile
     */
    public void createSettingsFile(File settingsFile) {
        List<String> contents = defaultSettings();
        List<String> fileContents = new ArrayList<>();
        try {
            settingsFile.createNewFile();
            contents.forEach((content) -> {
                fileContents.add(content);
            });
            Files.write(settingsFile.toPath(), fileContents, Charset.forName("UTF-8"));
        } catch (IOException ex) {
            //TODO
        }
    }

    /**
     * Reads the Settings text file and returns the value of a specified
     * setting.
     *
     * @return Value of Settings.
     */
    public String getSetting() {
        String line;
        String settingValue = null;

        try {
            BufferedReader buf_reader = new BufferedReader(new FileReader(file));
            while ((line = buf_reader.readLine()) != null) {
                String[] settingTagAndValue = line.split(seperator);
                if (settingTag.equals(settingTagAndValue[0].replace("\ufeff", ""))) {
                    buf_reader.close();
                    settingValue = settingTagAndValue[1];
                    break;
                }
            }
        } catch (FileNotFoundException ex) {
            //TODO
        } catch (IOException ex) {
            //TODO
        }

        return settingValue;
    }

    /**
     * Changes the a specified setting in the Settings text file.
     *
     */
    public void setSetting() {
        StringBuilder inputBuilder = new StringBuilder();
        String line;
        String oldSetting = null;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            while ((line = bufferedReader.readLine()) != null) {
                String[] setting = line.split(seperator);
                if (setting[0].equals(settingTag)) {
                    oldSetting = setting[1];
                }

                inputBuilder.append(line);
                inputBuilder.append('\n');
            }
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }

        String inputStr = inputBuilder.toString();
        inputStr = inputStr.replace(oldSetting, newSetting);

        try {
            FileOutputStream output = new FileOutputStream(file);
            output.write(inputStr.getBytes());
            output.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }

    /**
     *
     */
    private List<String> defaultSettings() {
        List<String> contents = new ArrayList<>();

        contents.add(Controller.TAG_DEFAULT_FOLDER 
                + seperator + Controller.VALUE_DEFAULT_FOLDER);

        return contents;
    }
}
