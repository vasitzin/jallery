package controller;

import db.DbConn;
import java.awt.Image;
import java.io.File;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.ImageIcon;
import model.Album;
import model.User;
import model.Picture;
import repository.album.AlbumFileRepository;
import repository.SettingsRepository;
import repository.UserRepository;
import repository.album.AlbumRepository;
import repository.picture.PictureFileRepository;
import repository.picture.PictureRepository;
import ui.AlbumCreator;
import ui.HomePage;
import ui.PictureView;

/**
 *
 * @author VaseiliosTzinis
 */
public class Controller {

    /**
     * Settings tags.
     */
    public static final String TAG_VALUE_SEPERATOR = " -> ";
    public static final String TAG_DEFAULT_FOLDER = "DEFAULT_FOLDER";

    /**
     * Settings values.
     */
    public static final String VALUE_DEFAULT_FOLDER = "C:\\";
    public static final int VALUE_THUMBNAIL_DIMENSION = 200;
    public static final String VALUE_ALBUMS_LOC = ".\\albums";
    public static final String VALUE_SETTINGS_LOC = ".\\settings.txt";

    /**
     * HomePage panel states.
     */
    public static final int LOADED_LOCAL_PICTURES = 1;
    public static final int LOADED_ONLINE_PICTURES = 2;
    public static final int LOADED_LOCAL_ALBUM_PICTURES = 3;
    public static final int LOADED_ONLINE_ALBUM_PICTURES = 4;
    public static final int LOADED_LOCAL_ALBUMS = 5;
    public static final int LOADED_ONLINE_ALBUMS = 6;

    private DbConn dbConn = new DbConn();
    private User user = new User();
    private int fcResult;
    private List<File> selectedFiles = new ArrayList<>();
    private int panelLoaded;
    private int thumbnailDimensions = VALUE_THUMBNAIL_DIMENSION;
    private String selectedFolder = new SettingsRepository(TAG_DEFAULT_FOLDER, "").getSetting();
    private List<Picture> pictureList = null;
    private PictureRepository picRep = new PictureRepository();
    private Album selectedAlbum = null;
    private List<Album> albumList = null;
    private AlbumRepository albumRep = new AlbumRepository();

    /**
     * Sets the pictureList/albumList with the appropriate list values.
     *
     * @param panelLoaded
     */
    public void loadList(int panelLoaded) {
        this.panelLoaded = panelLoaded;
        switch (panelLoaded) {
            case LOADED_LOCAL_PICTURES:
            case LOADED_ONLINE_PICTURES:
            case LOADED_LOCAL_ALBUM_PICTURES:
            case LOADED_ONLINE_ALBUM_PICTURES:
                loadPictureList();
                break;
            case LOADED_LOCAL_ALBUMS:
            case LOADED_ONLINE_ALBUMS:
                loadAlbumList();
                break;
            default:
                break;
        }
    }

    /**
     * Initializes the label that's used as a thumbnail for pictures/albums.
     *
     * @return an empty JLabel[] with the length of the pictureList/albumList
     */
    public int getListLength() {
        switch (panelLoaded) {
            case LOADED_LOCAL_PICTURES:
            case LOADED_ONLINE_PICTURES:
            case LOADED_LOCAL_ALBUM_PICTURES:
            case LOADED_ONLINE_ALBUM_PICTURES:
                return pictureList.size();
            case LOADED_LOCAL_ALBUMS:
            case LOADED_ONLINE_ALBUMS:
                return albumList.size();
            default:
                return 0;
        }
    }

    /**
     * Finds the path of the picture that should appear in the label icon of a
     * picture/album thumbnail in the HomePage panel.
     *
     * @param position picture or album list
     * @return the path of an image
     */
    public ImageIcon buildThumbnail(int position) {
        ImageIcon icon = findIcon(position);
        icon = new ImageIcon(icon
                .getImage()
                .getScaledInstance(thumbnailDimensions, thumbnailDimensions, Image.SCALE_SMOOTH));
        return icon;
    }

    /**
     *
     * @param position picture
     * @return the path of an image
     */
    public ImageIcon buildPicture(int position) {
        return findIcon(position);
    }

    /**
     * Depending on the panelLoaded finds appropriate picture.
     *
     * @param i position of picture in pictureList
     * @return specified picture in position
     */
    public Picture getPictureFromList(int i) {
        switch (panelLoaded) {
            case LOADED_LOCAL_PICTURES:
            case LOADED_ONLINE_PICTURES:
            case LOADED_LOCAL_ALBUM_PICTURES:
            case LOADED_ONLINE_ALBUM_PICTURES:
                return pictureList.get(i);
            default:
                return null;
        }
    }

    /**
     * Depending on the panelLoaded finds appropriate album.
     *
     * @param i position of album in albumList
     * @return specified album in position
     */
    public Album getAlbumFromList(int i) {
        switch (panelLoaded) {
            case LOADED_LOCAL_ALBUMS:
            case LOADED_ONLINE_ALBUMS:
                return albumList.get(i);
            default:
                return null;
        }
    }

    /**
     *
     * @param ac
     */
    public void createAlbum(AlbumCreator ac) {
        switch (ac.getSelectedOption()) {
            case AlbumCreator.MANUALLY:
                manualAlbumCreate(ac.getSelectedName(), ac.getSelectedFiles());
                break;
            case AlbumCreator.AUTO_DATE:
                Date selectedDate = Date.from(ac.getSelectedLocalDate()
                        .atStartOfDay()
                        .atZone(ZoneId.systemDefault())
                        .toInstant());
                Calendar selectedCalendar = Calendar.getInstance();
                selectedCalendar.setTime(selectedDate);
                autoDateAlbumCreate(ac.getSelectedName(), ac.getSelectedFiles(), selectedCalendar);
                break;
            default:
                break;
        }
    }

    /**
     * Deletes the specified Album's entire existance.
     *
     * @param position
     */
    public void eraseAlbum(int position) {
        switch (panelLoaded) {
            case LOADED_LOCAL_ALBUMS:
                albumRep.setAlbumRepository(new AlbumFileRepository(
                        new File(VALUE_ALBUMS_LOC + "\\" + albumList.get(position).getTitle())));
                albumRep.eraseAlbum();
                albumList.remove(position);
                break;
            case LOADED_ONLINE_ALBUMS:
                //TODO
                break;
            default:
                break;
        }
    }

    /**
     * Deletes the specified Picture from the current Album.
     *
     * @param position
     */
    public void removePictureFromAlbum(int position) {
        switch (panelLoaded) {
            case LOADED_LOCAL_ALBUM_PICTURES:
                albumRep.setAlbumRepository(new AlbumFileRepository(
                        new File(VALUE_ALBUMS_LOC + "\\" + selectedAlbum.getTitle())));
                albumRep.removePictureFromAlbum(albumRep.getPicturePath(position));
                pictureList.remove(position);
                break;
            case LOADED_ONLINE_ALBUM_PICTURES:
                //TODO
                break;
            default:
                break;
        }
    }

    /**
     * Upload picture to the cloud.
     */
    public void uploadPictures() {
        //TODO
    }

    /**
     *
     * @param username
     * @param password
     * @return specifies if action was succesful or not
     */
    public boolean signinRoutine(String username, String password) {
        UserRepository userRep = new UserRepository(dbConn, new User(0, username, ""));
        user = userRep.signInUser(password);
        if (user.isLogedIn()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param user
     * @param password
     * @return
     */
    public String signupRoutine(User user, String password) {
        UserRepository userRep = new UserRepository(dbConn, user);
        return userRep.signUpUser(password);
    }

    /**
     *
     * @return specifies if action was succesful or not
     */
    public boolean signoutRoutine() {
        switch (panelLoaded) {
            case LOADED_ONLINE_PICTURES:
            case LOADED_ONLINE_ALBUM_PICTURES:
            case LOADED_ONLINE_ALBUMS:
                user = new User();
                selectedFolder = new SettingsRepository(TAG_DEFAULT_FOLDER, "").getSetting();
                return true;
            default:
                user = new User();
                return false;
        }
    }

    /**
     * Changes the user specified default folder in the setting txt file.
     */
    public void changeDefaultFolder() {
        new SettingsRepository(TAG_DEFAULT_FOLDER, selectedFolder).setSetting();
    }

    /**
     * Changes the current default folder to the user specified folder.
     */
    public void gotoDefaultDestination() {
        selectedFolder = new SettingsRepository(TAG_DEFAULT_FOLDER, "").getSetting();
    }

    /**
     *
     * @param positionOfPicture
     */
    public void showPictureViewFrame(int positionOfPicture) {
        new PictureView(positionOfPicture, this).setVisible(true);
    }

    /**
     * Sets the pictureList with the appropriate list values.
     */
    private void loadPictureList() {
        switch (panelLoaded) {
            case LOADED_LOCAL_PICTURES:
                picRep.setPictureRepository(new PictureFileRepository(new File(selectedFolder)));
                pictureList = picRep.getAllPictures();
                break;
            case LOADED_ONLINE_PICTURES:
                //TODO
                break;
            case LOADED_LOCAL_ALBUM_PICTURES:
                pictureList = selectedAlbum.getPictures();
                break;
            case LOADED_ONLINE_ALBUM_PICTURES:
                //TODO
                break;
            default:
                break;
        }
        albumList = null;
    }

    /**
     * Sets the albumList with the appropriate list values.
     */
    private void loadAlbumList() {
        switch (panelLoaded) {
            case LOADED_LOCAL_ALBUMS:
                albumRep.setAlbumRepository(new AlbumFileRepository(new File(VALUE_ALBUMS_LOC)));
                albumList = albumRep.getAllAlbums();
                break;
            case LOADED_ONLINE_ALBUMS:
                //TODO
                break;
            default:
                break;
        }
        pictureList = null;
    }

    /**
     *
     * @param position
     * @return
     */
    private String getLocalPicturePath(int position) {
        return selectedFolder + "\\" + pictureList.get(position).getTitle();
    }

    /**
     *
     * @param position
     * @return
     */
    private String getLocalAlbumPicturePath(int position) {
        albumRep.setAlbumRepository(new AlbumFileRepository(
                new File(VALUE_ALBUMS_LOC + "\\" + selectedAlbum.getTitle())));
        return albumRep.getPicturePath(position);
    }

    /**
     *
     * @param position
     * @return
     */
    private String getLocalAlbumPath(int position) {
        albumRep.setAlbumRepository(new AlbumFileRepository(
                new File(VALUE_ALBUMS_LOC + "\\" + albumList.get(position).getTitle())));
        return albumRep.getThumbnailByAlbumId();
    }

    /**
     *
     */
    private void manualAlbumCreate(String title, List<File> files) {
        albumRep.setAlbumRepository(new AlbumFileRepository(
                new File(Controller.VALUE_ALBUMS_LOC, title + ".txt")));
        List<String> paths = new ArrayList<>();
        files.forEach((file) -> {
            paths.add(file.getPath());
        });
        albumRep.createAlbum(paths);
    }

    /**
     *
     */
    private void autoDateAlbumCreate(String title, List<File> files, Calendar selectedCalendar) {
        albumRep.setAlbumRepository(new AlbumFileRepository(
                new File(Controller.VALUE_ALBUMS_LOC, title + ".txt")));
        List<String> paths = new ArrayList<>();
        files.forEach((file) -> {
            picRep.setPictureRepository(new PictureFileRepository(file));
            Picture pic = picRep.getAPicture();
            Calendar picCalendar = Calendar.getInstance();
            try {
                picCalendar.setTime(pic.getDate());
                if (selectedCalendar.get(Calendar.MONTH) == picCalendar.get(Calendar.MONTH)
                        && selectedCalendar.get(Calendar.YEAR) == picCalendar.get(Calendar.YEAR)) {
                    paths.add(file.getPath());
                }
            } catch (NullPointerException ex) {
                //TODO picture with null date
            }
        });
        albumRep.createAlbum(paths);
    }

    /**
     *
     */
    private void manualOnlineAlbumCreate() {
        //TODO manual online album create
    }

    /**
     *
     */
    private void autoDateOnlineAlbumCreate() {
        //TODO auto by date online album create
    }

    private ImageIcon findIcon(int position) {
        ImageIcon icon;
        switch (panelLoaded) {
            case LOADED_LOCAL_PICTURES:
                icon = new ImageIcon(getLocalPicturePath(position));
                break;
            case LOADED_ONLINE_PICTURES:
                //TODO
                return null;
            case LOADED_LOCAL_ALBUM_PICTURES:
                icon = new ImageIcon(getLocalAlbumPicturePath(position));
                break;
            case LOADED_ONLINE_ALBUM_PICTURES:
                //TODO
                return null;
            case LOADED_LOCAL_ALBUMS:
                icon = new ImageIcon(getLocalAlbumPath(position));
                break;
            case LOADED_ONLINE_ALBUMS:
                //TODO
                return null;
            default:
                return null;
        }
        return icon;
    }

    // <editor-fold defaultstate="collapsed" desc="Getters 'n' Setters">
    public DbConn getDbConn() {
        return dbConn;
    }

    public void setDbConn(DbConn dbConn) {
        this.dbConn = dbConn;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getFcResult() {
        return fcResult;
    }

    public void setFcResult(int fcResult) {
        this.fcResult = fcResult;
    }

    public List<File> getSelectedFiles() {
        return selectedFiles;
    }

    public void setSelectedFiles(List<File> selectedFiles) {
        this.selectedFiles = selectedFiles;
    }

    public int getPanelLoaded() {
        return panelLoaded;
    }

    public void setPanelLoaded(int panelLoaded) {
        this.panelLoaded = panelLoaded;
    }

    public int getThumbnailDimensions() {
        return thumbnailDimensions;
    }

    public void setThumbnailDimensions(int thumbnailDimensions) {
        this.thumbnailDimensions = thumbnailDimensions;
    }

    public String getSelectedFolder() {
        return selectedFolder;
    }

    public void setSelectedFolder(String selectedFolder) {
        this.selectedFolder = selectedFolder;
    }

    public PictureRepository getPicRep() {
        return picRep;
    }

    public void setPicRep(PictureRepository picRep) {
        this.picRep = picRep;
    }

    public List<Picture> getPictureList() {
        return pictureList;
    }

    public void setPictureList(List<Picture> pictureList) {
        this.pictureList = pictureList;
    }

    public Album getSelectedAlbum() {
        return selectedAlbum;
    }

    public void setSelectedAlbum(Album selectedAlbum) {
        this.selectedAlbum = selectedAlbum;
    }

    public AlbumRepository getAlbumRep() {
        return albumRep;
    }

    public void setAlbumRep(AlbumRepository albumRep) {
        this.albumRep = albumRep;
    }

    public List<Album> getAlbumList() {
        return albumList;
    }

    public void setAlbumList(List<Album> albumList) {
        this.albumList = albumList;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="main method">
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
        * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HomePage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                preRunRoutine();
                new HomePage().setVisible(true);
            }
        });
    }
    // </editor-fold>

    /**
     * A method to be called before programm starts to ensure existance of: a)
     * the folder "albums" in which *ALBUM_NAME*.txt exist, b) the Settings.txt
     * file.
     */
    private static void preRunRoutine() {

        new File(VALUE_ALBUMS_LOC).mkdirs();

        if (!new File(VALUE_SETTINGS_LOC).exists()) {
            new SettingsRepository().createSettingsFile(new File(VALUE_SETTINGS_LOC));
        }
    }
}
